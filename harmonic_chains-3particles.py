#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 22:41:41 2017
@author: lovuit

So basically, what I am trying to do here is simulate simple chain, just mimicing 
the real bond-stretching potential in MD.
3 particles, with mass m1, m2, m3, two springs connecting them with force constants k12 and k23,
another two springs connecting atom1 to the "left wall", and atom2 to the "right wall"
with force constants k0 and k3.

No specific units are involved here. All the values are taken to be arbitrary
so only the relative values matters. If one wants the values to make sense physicall,
then specific unit systems may need to be adopted and values need to be taken accordingly.

the initial velocities are set to be some "small" values, ideally, take velocities 
from some Boltzmann disribution and with zero overall momentum will be a better choice.
But assume long time will "wash away" the initial condition, this effect may not count much.

"""


import numpy as np
import random
import matplotlib.pyplot as plt

m1 = 2
m2 = 1
m3 = 2 
k0 = 1.0
k12 = 2.0
k23 = 3.0
k3 = 1.0
x01 = 0.1
x012 = 0.2
x023 = 0.3
x03 = 0.4
gammaL = 1
gammaR = 1
TL = 10
TR = 1
kB = 1


tBegin = 0
tEnd = 10000.0
dt = 0.01
sqrtdt = np.sqrt(dt)
tArray = np.arange(tBegin, tEnd, dt)
tsize = tArray.size
Ntraj = 1 # take only one trajectory, and take long enough time to reach steady state

traj = 0
tstep = 0
while traj<Ntraj:
    f1new = 0
    f2new = 0
    f3new = 0
    
    x1 = np.zeros(tsize)
    x2 = np.zeros(tsize)
    x3 = np.zeros(tsize)

    v1 = np.zeros(tsize)
    v2 = np.zeros(tsize)
    v3 = np.zeros(tsize)
    
    P12 = np.zeros(tsize)
    P23 = np.zeros(tsize)
    
    U = np.zeros(tsize)
    K = np.zeros(tsize)

    x1[0] = 0.12
    x2[0] = 0.23
    x3[0] = 0.35

    v1[0] = 0.01
    v2[0] = 0.01
    v3[0] = 0.01
    while tstep < (tsize-1):
        
#        f1old = -k0*(x1[tstep]-x01) + k12*(x2[tstep]-x1[tstep]-x012)
#        f2old = -k12*(x2[tstep]-x1[tstep]-x012) + k23*(x3[tstep]-x2[tstep]-x023)
#        f3old = -k23*(x3[tstep]-x2[tstep]-x023) - k3*(x3[tstep]-x03)
        f1old = f1new
        f2old = f2new
        f3old = f3new        
 
#-----------EOM integrator: using the velocity verlet algorithm-----------------------
        x1[tstep+1] = x1[tstep] + v1[tstep]*dt + (0.5/m1)*f1old*dt**2
        x2[tstep+1] = x2[tstep] + v2[tstep]*dt + (0.5/m2)*f2old*dt**2
        x3[tstep+1] = x3[tstep] + v3[tstep]*dt + (0.5/m3)*f3old*dt**2

        f1new = -k0*(x1[tstep+1]-x01) + k12*(x2[tstep+1]-x1[tstep+1]-x012)
        f2new = -k12*(x2[tstep+1]-x1[tstep+1]-x012) + k23*(x3[tstep+1]-x2[tstep+1]-x023)
        f3new = -k23*(x3[tstep+1]-x2[tstep+1]-x023) - k3*(x3[tstep+1]-x03)

        v1[tstep+1] = v1[tstep] + 0.5*((f1old+f1new)/m1 - gammaL*(v1[tstep]+v1[tstep+1]))*dt + np.sqrt(2*kB*gammaL*TL/m1)/sqrtdt*random.gauss(0,1)*dt
        v2[tstep+1] = v2[tstep] + (0.5/m2)*(f2old+f2new)*dt
        v3[tstep+1] = v3[tstep] + 0.5*((f3old+f3new)/m3 - gammaR*(v3[tstep]+v3[tstep+1]))*dt + np.sqrt(2*kB*gammaR*TR/m3)/sqrtdt*random.gauss(0,1)*dt
#----------------------------------------------------------------------------------

        if tstep>(tsize/2): #take average after later half of timesteps, so the data might more close to steady state
            P12[tstep+1] = f1new*v1[tstep+1] - f2new*v2[tstep+1]
            P23[tstep+1] = f2new*v2[tstep+1] - f3new*v3[tstep+1]

        U[tstep] = 0.5*k12*(x2[tstep]-x1[tstep]-x012)**2 + 0.5*k23*(x3[tstep]-x2[tstep]-x023)**2 + 0.5*k0*(x1[tstep]-x01)**2 + 0.5*k3*(x3[tstep]-x03)**2        
         # The potential energy of first and last springs connecting atom1 and atom3 are taken into account to total potential
         
        K[tstep] = 0.5*m1*v1[tstep]**2 + 0.5*m2*v2[tstep]**2 + 0.5*m3*v3[tstep]**2

#        if tstep % 1000 == 0:
#            print tstep,x2[tstep],v2[tstep],f1old,f1new

        tstep += 1
        
        
    traj += 1 

    print "trajectory number =", traj
    sumK = np.sum(K)
    sumU = np.sum(U)
    sumE = sumK + sumU

#plt.plot(tArray,P23)
print sumK, sumU, sumE/tsize, np.sum(P12)/(tsize/2), np.sum(P23)/(tsize/2)


       
    
